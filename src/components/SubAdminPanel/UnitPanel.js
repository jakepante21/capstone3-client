import React,{Fragment} from "react";
import AddUnitForm from "./../forms/AddUnitForm";
import EditUnitForm from "./../forms/EditUnitForm";

const UnitPanel = ({products,rooms}) =>{
	return(
		<Fragment>
			<section className="sub-admin-panel">
				<AddUnitForm products={products}/>
				<EditUnitForm rooms={rooms}/>
			</section>
		</Fragment>
	)
}

export default UnitPanel;