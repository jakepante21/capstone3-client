import React, {Fragment} from "react";
import AddCategoryForm from "./../forms/AddCategoryForm";
import EditCategoryForm from "./../forms/EditCategoryForm";
import DeleteCategoryForm from "./../forms/DeleteCategoryForm";

const CategoryPanel = ({categories,handleChangeCategoriesStatus,categoriesStatus}) =>{
	return(
		<Fragment>
			<section className="sub-admin-panel">
				<AddCategoryForm/>
				<EditCategoryForm categories = {categories} handleChangeCategoriesStatus = {handleChangeCategoriesStatus} categoriesStatus = {categoriesStatus}/>
				<DeleteCategoryForm categories = {categories} handleChangeCategoriesStatus = {handleChangeCategoriesStatus} categoriesStatus = {categoriesStatus}/>
			</section>
		</Fragment>
	)
}

export default CategoryPanel;