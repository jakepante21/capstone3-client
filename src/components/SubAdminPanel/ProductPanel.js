import React, {Fragment} from "react";
import AddProductForm from "./../forms/AddProductForm";
import EditProductForm from "./../forms/EditProductForm";
import DeleteProductForm from "./../forms/DeleteProductForm";

const ProductPanel = ({categories,products}) =>{
	return(
		<Fragment>
			<section className="sub-admin-panel">
				<AddProductForm categories={categories}/>
				<EditProductForm categories={categories} products={products}/>
				<DeleteProductForm products={products}/>
			</section>
		</Fragment>
	)
}

export default ProductPanel;