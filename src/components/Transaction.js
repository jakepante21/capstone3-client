import React from "react";
import TransactionForm from "./forms/TransactionForm";

const Transaction = ({transactions,rooms}) => {
	return(
		<div className="general-h">
			<TransactionForm transactions={transactions} rooms={rooms}/>
		</div>
	)
}

export default Transaction;