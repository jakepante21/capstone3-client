import React,{Fragment,useState,useReducer,useEffect}from "react";
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {DateRangeInput, DateSingleInput, Datepicker} from '@datepicker-react/styled';
import { ThemeProvider } from "styled-components";
import headerImage from "./images-videos/home1.jpg";
import Pagination from "./Pagination";

const initialState = {
		startDate: null,
	  	endDate: null,
	  	focusedInput: null,
	}

function reducer(state, action) {
  	switch (action.type) {
    	case 'focusChange':
      		return {...state, focusedInput: action.payload}
    	case 'dateChange':
      		return action.payload
   	 	default:
      		throw new Error()
  }
}

const Home =({products,addToCart,transactions,date,reservedDates,rooms,dates,cart})=>{

	const zeroDate = new Date();
	const firstDate = zeroDate.getFullYear() + "-" + 0 + parseInt(zeroDate.getMonth() + 1) + "-" + zeroDate.getDate();
	const secondDate = zeroDate.getFullYear() + "-" + 0 + parseInt(zeroDate.getMonth() + 1) + "-" + parseInt(zeroDate.getDate() + 1);
	const [state, dispatch] = useReducer(reducer, initialState);
	const [startDate, setStartDate] = useState(firstDate);
  	const [endDate, setEndDate] = useState(secondDate);

	// const [dateReserved,setDateReserved] = useState(new Date("2020-02-12"));

	const [finalDate,setFinalDate] = useState([]);

	const [newRooms,setNewRooms] = useState([]);

	const [newProducts,setNewProducts] = useState([]);

	const [stateDate,setStateDate] = useState();

	const [pickDate,setPickDate] = useState([]);

	const [anotherDate,setAnotherDate] = useState([]);

	const [currentPage,setCurrentPage] = useState(1);

	const [postsPerPage,setPostsPerPage] = useState(5);

	const [category,setCategory] = useState("All");

	const [popupCart,setPopupCart] = useState([]);

	const [numberOfPerson,setNumberOfPerson] = useState({
		Adult : null,
		Child : 0
	});

	// useEffect( ()=>{
	// 	transactions.map(transaction=>{
	// 		if(transaction.status == "Accepted"){
	// 			setDateReserved({...dateReserved,transactionCode : transaction.transactionCode,startDate : transaction.startDate , endDate : transaction.endDate})
	// 			// setDateReserved(new Date(transaction.startDate) + 1)
	// 		}

	// 	})

		
		
	// },[])

	const changeNumberOfPerson = (e) =>{
		// console.log(e.target.value);
		// console.log(e.target.name);
		setNumberOfPerson({...numberOfPerson,[e.target.name] : e.target.value})

	}

	const handleSetCategory = (e) =>{
		setCategory(e.target.value);
	}

	const startDateHandle = (e) =>{
		// console.log(e.target.value)
		setStartDate(e.target.value);
	}
	const endDateHandle = (e) =>{
		// console.log(e.target.value)
		setEndDate(e.target.value);
	}

	const getBetweenDates = (start,end) => {
		let arrDates = [];
		let sDate = new Date(start);
		let eDate = new Date(end);
		while( sDate <= eDate){
			arrDates.push(new Date(sDate))
			sDate.setDate(sDate.getDate() + 1);

		}
		localStorage.setItem("days",arrDates.length);
		return arrDates;
	} 

	const handleSetPickDate = (first,second) =>{
		let newArrayDate = getBetweenDates(first,second)
		let newNewArrayDate = newArrayDate.map(a=>{
			return a
		})
		setPickDate(newNewArrayDate)
	}

	const filterRoom = () =>{
		let dateReserved = reservedDates.find(resDate => {
			let a = resDate.dates.map(datess=>{
				return {
					date : datess.date
				}
			})
			// console.log(resDate)
			return a
			// console.log(a)
		})

		// let c = reservedDates.map( reservedDate => {
		// 	let y = reservedDate.dates.map( date => {
		// 		let x =pickDate.filter( test => {
		// 			return (new Date(date.date).getTime() == new Date(test).getTime());
		// 		})
		// 		return x;
		// 	})
		// 	return y;
		// })



		let c = [];
		reservedDates.forEach( reservedDate => {
			let y = reservedDate.dates.forEach( date => {
				let x =pickDate.forEach( test => {
					// console.log(new Date(date.date).getTime())
					// console.log(new Date(date.date).getTime() == new Date(test).getTime())
					if (new Date(date.date).getTime() == new Date(test).getTime()){
						c = [...c,reservedDate.roomNumber];
					};
				})
			})

		})

		if(c.length < 1){
			setNewRooms(rooms)
			let peopleSum = parseInt(numberOfPerson.Adult) + parseInt(numberOfPerson.Child);
			// console.log(rooms)
			let finalProductRoom = [];

			products.forEach( proRoom => {
				if(proRoom.maxPerson >= peopleSum){
					finalProductRoom.push(proRoom);
				}
			})

			setNewProducts(finalProductRoom);

		}else{
			let b = [];
			let d = "";
			c.forEach(bagoRoom=>{
				if(bagoRoom !== d){
					b = [...b,bagoRoom];
				}
				d = bagoRoom;
			})
			let f = [];
			// b.forEach(bagoRoom=>{
			// 	rooms.forEach(room=>{
			// 		if(bagoRoom !== room.roomNumber){
			// 			if(!b.includes(room.roomNumber)){	
			// 				if(!f.includes(room.roomNumber)){
			// 					f = [...f,room];
			// 				}
			// 			}
			// 		}
			// 	})
				
			// })
			let newArray = [];

			rooms.forEach(room => {
			  if (b.indexOf(room.roomNumber) === -1) {
			    newArray = [...newArray, room];
			  }
			})

			let peopleSum = parseInt(numberOfPerson.Adult) + parseInt(numberOfPerson.Child);

			let productRoom = products.filter(prodRoom => {
				let a = newArray.find(newArrayRoom => {
					return prodRoom._id === newArrayRoom.productId;
				})
				return a;
			})

			let finalProductRoom = [];

			productRoom.forEach( proRoom => {
				if(proRoom.maxPerson >= peopleSum){
					finalProductRoom.push(proRoom);
				}
			})
			
			// console.log(newArray);
			setNewProducts(finalProductRoom);

			setNewRooms(newArray)
		}



		// console.log(c)
		
		// let a = rooms.filter(room=>{
		// 	return room.roomNumber !== c[0]
		// })
		// console.log(b)
		
		// let aaaa = rooms.forEach(room=>{ 
		// 	let bbb = b.find(bagoRoom=>{
		// 		return room.roomNumber !== bagoRoom
		// 	})
		// 	return bbb
		// })
		// console.log(f)
		// console.log(qwe)
		// console.log(a)
		
		// let sample1 =  new Date(dateReserved.dates[0].date).getTime();
		// let sample2 =  new Date(pickDate[0]).getTime();



		// console.log("sample1", sample1)
		// console.log("sample2", sample2)

		// console.log(sample2 == sample1)
		// let a = new Date(dateReserved)
		// console.log(dateReserved)

		// let filteredRooms = pickDate.map(pick=>{
		// 		let c = dateReserved.map(days=>{
		// 			let b = days.filter(day=>{
		// 				if(pick.date !== day){
		// 					return day
		// 				}
		// 				else{
		// 					console.log("nope")
		// 				}
		// 			})

		// 			return b
		// 		})
		// 		return c
		// })

		// a = [

		// ]
		// b = []

		// console.log(filteredRooms)

		// resDate.dates.map(etc=>{
				// 	pickDate.filter(pick=>{
				// 		if(pick.date !== etc.date){
				// 			console.log(pick.date)
				// 		}
				// 		else{
				// 			console.log("hoyo")
				// 		}
				// 	})
				// })
				// let a = rooms.filter(room=>{
				// 	return room.roomNumber !== resDate.roomNumber
				// })
				// setNewRooms(a)

	}

	useEffect(()=>{
		handleSetPickDate(startDate,endDate)
	},[startDate,endDate])

	const showRooms = () =>{
		if(startDate && endDate){
			// let a = new Date(pickDate[1])
			filterRoom()
		}
		else{
			// console.log("heya")
		}

		let roomTypeFilter = document.querySelector(".room-type-filter-container");
		roomTypeFilter.style.display = "flex";
		
	}

	// console.log(cart)

	const handleDate = () =>{
		setStateDate()
	}

	useEffect(() => {
		localStorage.setItem("pickedDate",JSON.stringify({startDate : startDate,endDate : endDate}))
	},[startDate,endDate])

	// console.log(dateReserved.startDate)

	// const changeButton = (e) =>{
	// 	let id = e.target.id
	// 	document.getElementById(id).innerHTML="Added";
	// 	document.getElementById(id).classList.remove("btn-warning");
	// 	document.getElementById(id).classList.add("btn-success");
	// }

	// const sendProduct = (product) => {
	// 	date(product)
	// 	changeButton()
	// }

	// let func = document.getElementById()

	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;
	const currentPosts = newRooms.slice(indexOfFirstPost,indexOfLastPost);

	const paginate = pageNumber => setCurrentPage(pageNumber);

	let popCart = document.querySelector(".pop-cart-container");
	let click = document.querySelector(".click");

	if(cart.length > 0){
		// popCart.style.display = "block";
		// popCart.style.transform = "translateX(" + 0 + "px)";
		// popCart.style.transition = "all 1s ease-in-out";
		popCart.classList.add("pop-cart-container-active");
	}

	const handlePopCart = () => {
		popCart.classList.toggle("pop-cart-container-minimize");
		click.classList.toggle("toggle");
	}

	const handleAddToPopCart = (a,b) => {
		setPopupCart([...popupCart,{name: a,type: b}])
		// console.log(a,b);
	}

	// console.log(popupCart)

	// console.log(startDate);
	// console.log(endDate);

	// console.log(currentPosts);
	// console.log(newProducts);

	return (
		<Fragment>
			<div className="heading">
				<div className="header-container">
					<img src={headerImage}/>
				</div>
				<div className="header-text-container">
					<h1>Book Here</h1>
				</div>
			</div>
			<section id="home-section" className="home-section">
				<div className="first-home-section">
					<div className="check-in-container">
						<h6>Check In:</h6>
						<input type="date" id="check-in" value={startDate} min={startDate} onChange={startDateHandle}/>
					</div>
					<div className="check-out-container">
						<h6>Check Out:</h6>
					    <input type="date" id="check-out" value={endDate} min={startDate} onChange={endDateHandle}/>
					</div>
					<div className="adult-container">
						<h6>Adult/s:</h6>
						<select name="Adult" onChange={changeNumberOfPerson}>
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
					<div className="child-container">
						<h6>Child:</h6>
						<select name="Child" onChange={changeNumberOfPerson}>
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
					<div className="check-button-container">
						<button id="button-hover2" onClick={showRooms}>Check Available Rooms</button>
					</div>
				</div>
				<div className="second-home-section">
					<div className="room-type-filter-container">
						<div className="room-type-title">
							<h6>Type of Room:</h6>
						</div>
						<select className="room-type-select" onChange={handleSetCategory}>
							<option value="All">All</option>
							<option value="Standard">Standard</option>
							<option value="Suite">Suite</option>
							<option value="Deluxe">Deluxe</option>
						</select>
					</div>
					{
						currentPosts.map(newRoom => {
							return (
								newProducts.map(room => {
									return(
										category === "All" ?
											newRoom.productId === room._id ?
												<div className="product-container">
													<div key={room._id} className="image-container">
														<img src={"https://bookacar.herokuapp.com" + room.image} alt="room image" className="room-image"/>
													</div>
													<div className="content-container">
														<div className="name-container">
															<h3>{room.name}</h3>
														</div>
														<hr/>
														<div className="description-container">
															<p>{room.description}</p>
														</div>
														<div className="price-container">
															<h6><span>Price Per Night: </span><span>&#8369;</span>{room.price.toFixed(2)}</h6>
														</div>
														<div className="type-container">
															<h6><span>Room Type: </span>{newRoom.type}</h6>
														</div>
														<div className="type-container">
															<h6><span>Max Person: </span>{room.maxPerson}</h6>
														</div>
														<div className="cart-button-container">
															<button id="button-hover2" onClick={ () => {addToCart(room,newRoom.type,newRoom.roomNumber,newRoom._id); handleAddToPopCart(room.name,newRoom.type)}}>Add to Cart</button>
														</div>
														<div className="book-button-container">
															<button id="button-hover2" onClick={()=>{addToCart(room,newRoom.type,newRoom.roomNumber,newRoom._id); date(startDate,endDate); window.location.href="/cart"}}>Book this Room</button>
														</div>
													</div>
												</div>
											:
											""
										:
										newRoom.productId === room._id ?
											newRoom.type === category ?
												<div className="product-container">
													<div key={room._id} className="image-container">
														<img src={"https://bookacar.herokuapp.com" + room.image} alt="room image" className="room-image"/>
													</div>
													<div className="content-container">
														<div className="name-container">
															<h3>{room.name}</h3>
														</div>
														<hr/>
														<div className="description-container">
															<p>{room.description}</p>
														</div>
														<div className="price-container">
															<h6><span>Price Per Night: </span><span>&#8369;</span>{room.price.toFixed(2)}</h6>
														</div>
														<div className="type-container">
															<h6><span>Room Type: </span>{newRoom.type}</h6>
														</div>
														<div className="cart-button-container">
															<button id="button-hover2" onClick={ () => {addToCart(room,newRoom.type,newRoom.roomNumber,newRoom._id); handleAddToPopCart(room.name,newRoom.type)}}>Add to Cart</button>
														</div>
														<div className="book-button-container">
															<button id="button-hover2" onClick={()=>{addToCart(room,newRoom.type,newRoom.roomNumber,newRoom._id); date(startDate,endDate); window.location.href="/cart"}}>Book this Room</button>
														</div>
													</div>
												</div>
											:
											""
										:
										""
									)
								})
							)
						})
					}
				</div>
				<Pagination totalPosts={newRooms.length} postsPerPage={postsPerPage} paginate={paginate}/>
			</section>
			<div className="pop-cart-container">
				<div className="cart-title-container">
					<h6>Room Cart</h6>
					<div className="click" onClick={handlePopCart}>
						<div className="line1"></div>
						<div className="line2"></div>
						<div className="line3"></div>
					</div>
				</div>
				<div className="cart-list-container">
					<ul>
						{
							popupCart.map(room => {
								return(
									<li>
										<h6>Room Name: {room.name}</h6>
										<span>Type: {room.type}</span>
									</li>
								)
							})
						}
					</ul>
				</div>
				<div className="book-button-popup-container">
					<button id="button-hover" onClick={()=>{date(startDate,endDate); window.location.href="/cart"}}>Book this Room/s</button>
				</div>
			</div>
		</Fragment>
	);
}


export default Home;
