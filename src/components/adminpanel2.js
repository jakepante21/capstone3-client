import React from "react";
import ProductPanel from "./SubAdminPanel/ProductPanel";
import CategoryPanel from "./SubAdminPanel/CategoryPanel";
import UnitPanel from "./SubAdminPanel/UnitPanel";

const AdminPanel = ({categories,products,handleChangeCategoriesStatus,categoriesStatus}) =>{
	return(
		<React.Fragment>
			<div className="container general-h">
				<div className="jumbotron bg-secondary">
					<h1 className="text-center text-light">Welcome To Admin Panel</h1>
					<h3 className="text-center text-light">Select what you want to manipulate</h3>
				</div>
				<div className="row">
					<div className="col-12 col-md-6 col-lg-4">
						<button className="btn btn-dark w-100" type="button" data-toggle="collapse" data-target="#productCrud" aria-expanded="false" aria-controls="collapseExample">Room CRUD</button>
					</div>
					<div className="col-12 col-md-6 col-lg-4">
						<button className="btn btn-dark w-100" type="button" data-toggle="collapse" data-target="#categoryCrud" aria-expanded="false" aria-controls="collapseExample">Category CRUD</button>
					</div>
					<div className="col-12 col-md-6 col-lg-4">
						<button className="btn btn-dark w-100" type="button" data-toggle="collapse" data-target="#unitCrud" aria-expanded="false" aria-controls="collapseExample">Add Unit</button>
					</div>
				</div>
				<div className="row">
					<div className="col-12">
						<div id="parentCrud">
							<div className="collapse" id="productCrud" data-parent="#parentCrud">
								<div className="card card-body">
								    <ProductPanel categories={categories} products={products}/>
								</div>
							</div>
							<div className="collapse" id="categoryCrud" data-parent="#parentCrud">
								<div className="card card-body">
								    <CategoryPanel categories = {categories} handleChangeCategoriesStatus = {handleChangeCategoriesStatus} categoriesStatus = {categoriesStatus}/>
								</div>
							</div>
							<div className="collapse" id="unitCrud" data-parent="#parentCrud">
								<div className="card card-body">
								    <UnitPanel products={products}/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}

export default AdminPanel;