import React from "react";

const Footer = () =>{
	return(
		<React.Fragment>
			<footer>
				<div className="footer-container">
					<p>
						All rights reserved &copy; 2019 | Jake Pante
					</p>		
				</div>
			</footer>
		</React.Fragment>
	)
}

export default Footer;