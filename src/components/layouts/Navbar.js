import React, {Fragment} from "react";
import {Link} from "react-router-dom";


const Navbar = () =>{
	let user = localStorage.getItem("user");
	let id = JSON.parse(user);
	if(localStorage.getItem("token")){
		if(id.role === "admin"){
			return (
				<Fragment>
					<nav className="navbar navbar-dark navbar-expand-sm">
						<Link to="/" className="nav-title">Hotel California</Link>
						<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
							<span className="navbar-toggler-icon"></span>
						</button>
						<div className="collapse navbar-collapse" id="navbarNav">
							<ul className="navbar-nav ml-auto">
								<li className="nav-item">
									<Link to="/" className="nav-link">Home</Link>
								</li>
								<li className="nav-item">
									<Link to="/admin-panel" className="nav-link">Admin Panel</Link>
								</li>
								<li className="nav-item">
									<Link to="/transactions" className="nav-link">Reservations</Link>
								</li>
								<li className="nav-item">
									<Link to="" className="nav-link">Welcome {id.firstname}!</Link>
								</li>
								<li className="nav-item">
									<Link to="/login" className="nav-link" onClick={ ()=>{
										localStorage.clear()
										window.location.href="/login"
									}}>Logout</Link>
								</li>
							</ul>
						</div>
					</nav>
				</Fragment>
			);
		}else{
			return (
				<Fragment>
					<nav className="navbar navbar-dark navbar-expand-sm">
						<Link to="/" className="nav-title">Hotel California</Link>
						<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
							<span className="navbar-toggler-icon"></span>
						</button>
						<div className="collapse navbar-collapse" id="navbarNav">
							<ul className="navbar-nav ml-auto">
								<li className="nav-item">
									<Link to="/" className="nav-link">Home</Link>
								</li>
								<li className="nav-item">
									<Link to="/book" className="nav-link">Book Here</Link>
								</li>
								<li className="nav-item">
									<Link to="/cart" className="nav-link">Cart</Link>
								</li>
								<li className="nav-item">
									<Link to="/transactions" className="nav-link">Reservations</Link>
								</li>
								<li className="nav-item">
									<Link to="" className="nav-link">Welcome {id.firstname}!</Link>
								</li>
								<li className="nav-item">
									<Link to="/login" className="nav-link" onClick={ ()=>{
										localStorage.clear()
										window.location.href="/login"
									}}>Logout</Link>
								</li>
							</ul>
						</div>
					</nav>
				</Fragment>
			);
		}
	}
	else{
		return (
			<Fragment>
				<nav className="navbar navbar-dark navbar-expand-sm">
					<Link to="/" className="nav-title">Hotel California</Link>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
						<span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse" id="navbarNav">
						<ul className="navbar-nav ml-auto">
							<li className="nav-item">
									<Link to="/" className="nav-link">Home</Link>
								</li>
							<li className="nav-item">
								<Link to="/book" className="nav-link">Book Here</Link>
							</li>
							<li className="nav-item">
								<Link to="/register" className="nav-link">Register</Link>
							</li>
							<li className="nav-item">
								<Link to="/login" className="nav-link">Login</Link>
							</li>
						</ul>
					</div>
				</nav>
			</Fragment>
		);
	}

	
}

export default Navbar;
