import React, {Fragment} from "react";
import {Link} from "react-router-dom";

const Nav = () =>{

	const burgerHandler = () =>{
		let nav = document.querySelector(".nav-links");
		let navLinks= document.querySelectorAll(".nav-links li")
		let burger = document.querySelector(".burger");

		nav.classList.toggle("nav-active");

		navLinks.forEach((link,index) => {
			if(link.style.animation){
				link.style.animation = "";
			}else{
				link.style.animation = "navLinkFade 0.5s ease forwards " + index / 7 + "s";
			}
			// console.log(index/ 7 + 1.5);
		});

		burger.classList.toggle("toggle");
	}

	let user = localStorage.getItem("user");
	let id = JSON.parse(user);
	if(localStorage.getItem("user")){
		return (
			<Fragment>
				<nav>
					<div className="logo">
						<h4>Hotel California</h4>
					</div>
					<ul className="nav-links">
						{
							id.role === "user" ? 
							<li>
								<Link to="/book" className="link">Home</Link>
							</li> :
							""
						}
						<li>
							<Link to="/" className="link">About Us</Link>
						</li>
						<li>
							<Link to="/contact" className="link">Contact Us</Link>
						</li>
						<li>
							<Link to="/transactions" className="link">Reservations</Link>
						</li>
						{
							id.role === "user" ?
							<li>
								<Link to="/cart" className="link">Cart</Link>
							</li> :
							<li>
								<Link to="/admin-panel" className="link">Admin Panel</Link>
							</li>
						}
						<li>
							<Link to="#" className="user-name">User</Link>
						</li>
						<li>
							<Link to="/login" className="logout-button" onClick={ ()=>{ localStorage.clear()
								window.location.href="/login"}}>Logout</Link>
						</li>
					</ul>
					<div className="burger" onClick={burgerHandler}>
						<div className="line1"></div>
						<div className="line2"></div>
						<div className="line3"></div>
					</div>
				</nav>
			</Fragment>
		)
	}else{
		return (
			<Fragment>
				<nav>
					<div className="logo">
						<h4>Hotel California</h4>
					</div>
					<ul className="nav-links">
						<li>
								<Link to="/book" className="link">Home</Link>
							</li>
						<li>
							<Link to="/" className="link">About Us</Link>
						</li>
						<li>
							<Link to="/contact" className="link">Contact Us</Link>
						</li>
						<li>
							<Link to="/register" className="link">Register</Link>
						</li>
						<li>
							<Link to="/login" className="link">Login</Link>
						</li>
					</ul>
					<div className="burger" onClick={burgerHandler}>
						<div className="line1"></div>
						<div className="line2"></div>
						<div className="line3"></div>
					</div>
				</nav>
			</Fragment>
		)
	}
	
}

export default Nav