import React, {Fragment,useEffect,useState} from "react";
import CartForm from "./forms/CartForm";

const Cart = ({carts,date,products,handleClearCart,handleRemoveItem,total}) =>{
    
	let localCart = [];	

	// console.log(localStorage.getItem("cart"))	

    	if(localStorage.getItem("cart") !== null){
    		console.log("wews")
	        const newCart = localStorage.getItem("cart");
	        localCart = JSON.parse(newCart);
	        console.log(localCart)
	    }else{
	        // let localCart =[];
	        console.log("what?")
	    }

	return (
		<Fragment>
			<span></span>
			<div className="cart-container">
				{
					localCart.length ?
						<CartForm cart={carts} date={date} products={products} handleClearCart={handleClearCart} handleRemoveItem={handleRemoveItem} total = {total}/>
					:
						<div className="cart-header">
							<h1 className="cart-title">Cart Empty!</h1>
						</div>
				}
			</div>
		</Fragment>
	)
}

export default Cart