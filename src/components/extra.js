<div className="container general-h">
					<div className="row row-color">
						<div className="col-12 col-md-3">
							<h6>Check In:</h6>
							<DatePicker
						        selected={startDate}
						        onChange={date => setStartDate(date)}
						        selectsStart
						        startDate={startDate}
						        endDate={endDate}
						        minDate={startDate}
						        className="start-date"
						    />
						</div>
						<div className="col-12 col-md-3">
							<h6>Check Out:</h6>
						    <DatePicker
						        selected={endDate}
						        onChange={date => setEndDate(date)}
						        selectsEnd
						        startDate={startDate}
						        endDate={endDate}
						        minDate={startDate}
						        className="end-date"
						    />
						</div>
						<div className="col-12 col-md-3">
							<button type="button" className="btn btn-warning home-button" onClick={showRooms}>Check Available Rooms</button>
						</div>
						<div className="col-12 col-md-3">
							<button className="btn btn-primary home-button" onClick={()=>{date(startDate,endDate)}}>Book this date</button>
						</div>
					</div>
					
					{
						newRooms.map(newRoom => {
							return (
								products.map(room => {
									return(
										newRoom.productId === room._id ?
											<div className="row">
												<div className="col-12 col-lg-8">
													<div className="room-container">
														<img src={"http://localhost:3001" + room.image} className="room-image"/>
														<div className="room-text">
															<h2>{room.name}</h2>
															<p>{room.description}</p>
															<p>&#8369; {room.price}</p>
															<button className="btn btn-primary" onClick={ () => {addToCart(room)}}>Add to Cart</button>
														</div>
														
													</div>
												</div>
											</div>
										:
										""
									)
								})
							)
						})
					}
					
				</div>