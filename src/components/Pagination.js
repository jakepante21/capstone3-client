import React,{Fragment} from "react";

const Pagination = ({totalPosts,postsPerPage,paginate}) =>{

	const pageNumbers = [];

	const pageColor = (e) =>{
		let pageNum = document.querySelector("#p" + e);
		let pageNums = document.querySelectorAll(".pages li")
		pageNums.forEach((link,index) => {
			if(link.childNodes[0].id !== pageNum.id){
				link.childNodes[0].classList.remove("page-color");
			}
		})
		pageNum.classList.add("page-color");
		// console.log(newPageNum)
		// if(newPageNum){
		// 	console.log(pageNum)
		// 	if(newPageNum !== pageNum){
		// 		console.log("true")
		// 		newPageNum.classList.remove("page-color");
		// 		// newPageNum = pageNum;
		// 		console.log(newPageNum)
		// 	}
		// }else{
		// 	newPageNum = pageNum;
		// 	console.log(newPageNum)
		// }
	}

	for(let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++){
		pageNumbers.push(i);
	}

	return(
		<Fragment>
			<div className="pagination-container">
				<ul className="pages">
					{
						pageNumbers.map(pageNumber =>{
							return(
								<li key={pageNumber}>
									<a href="#" id={"p" + pageNumber} onClick={() => {paginate(pageNumber); pageColor(pageNumber)}}>{pageNumber}</a>
								</li>
							)
						})
					}
				</ul>
			</div>
		</Fragment>
	)
}

export default Pagination;