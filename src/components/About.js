import React, {Fragment} from "react";
import videoOne from "./images-videos/home2.mp4";
import firstImage from "./images-videos/about1.jpg";
import secondImage from "./images-videos/about3.jpg";
import thirdImage from "./images-videos/about4.jpg";
import fourthImage from "./images-videos/about2.jpg";
import fifthImage from "./images-videos/index1.jpg";

const About = () => {
	return(
		<Fragment>
			<div className="heading">
				<div className="header-container">
					<video autoPlay loop muted>
						<source src={videoOne} type="video/mp4" />
					</video>
				</div>
				<div className="header-text-container">
					<h1>About Us</h1>
				</div>
			</div>
			<section className="section">
				<div className="about-first-caption">
					<p>
						<span>Hotel California</span>
						<hr/>
						is the fastest growing company in the hotel industry in the Philippines. Started on 2017, now with four hotels in Metro Manila.
					</p>
				</div>
				<div className="first-about-section">
					<div className="first-image">
						<div className="overlay1">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum tempus diam, et pharetra justo viverra eget.
							</p>
						</div>
						<img src={firstImage} />
					</div>
					<div className="second-image">
						<div className="overlay2">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum tempus diam, et pharetra justo viverra eget.
							</p>
						</div>
						<img src={secondImage} />
					</div>
					<div className="third-image">
						<div className="overlay3">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum tempus diam, et pharetra justo viverra eget.
							</p>
						</div>
						<img src={thirdImage} />
					</div>
					<div className="fourth-image">
						<div className="overlay4">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum tempus diam, et pharetra justo viverra eget.
							</p>
						</div>
						<img src={fourthImage} />
					</div>
					<div className="fifth-image">
						<div className="overlay5">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum tempus diam, et pharetra justo viverra eget.
							</p>
						</div>
						<img src={fifthImage} />
					</div>
				</div>
				<div className="second-about-section">
					<div className="mission-card">
						<h6>Mission</h6>
						<hr/>
						<p>
							The mission of the Hotel California is to put hospitality services on the highest level in order to satisfy the demands and expectations of guests. Our aim is to make the Hotel Grand a place for encounters, business success, pleasant meetings and gala ceremonies.
						</p>
					</div>
					<div className="vision-card">
						<h6>Vision</h6>
						<hr/>
						<p>
							The ideology of our vision is to continue to apply and set the highest standards of service quality and in that way justify and uphold the reputation that we have among the guests, partners, competitors and the wider community. We use and constantly introduce environmentally friendly technologies and processes in order to remain in balance with nature and also meet the needs of contemporary society.
						</p>
					</div>
				</div>
			</section>
		</Fragment>
	)
}

export default About;