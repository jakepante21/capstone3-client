import React, {useState,Fragment} from "react";


const DeleteCategory = ({categories,deleteCategory,handleChangeCategoriesStatus,categoriesStatus}) =>{

	const [selectedCategory,setSelectedCategory] = useState({
		name : null,
		id : null
	})

	const handleChangeSelected = (e) => {
		let name = categories.find(category=>{
			return category._id === e.target.value
		})
		setSelectedCategory({
			id : e.target.value,
			name : name.name
		})
	}

	const handleDelete = (e) =>{
		e.preventDefault();
		fetch("https://bookacar.herokuapp.com/categories/"+selectedCategory.id,{
			method : "DELETE",
			body : JSON.stringify({name : selectedCategory.name}),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			}
		})
		.then(data => data.json())
		.then(category => handleChangeCategoriesStatus({
			lastUpdated : selectedCategory.id,
			status : "PASS!",
			isLoading : true
		}))
	}

	return(
		<div className="add-product-container mt-3">
			<form onSubmit={handleDelete}>
				<div className="add-room-title">
					<h1>Delete Category</h1>
				</div>
				{
					categoriesStatus.isLoading ? 
					<div className="spinner-border" role="status">
						<span className="sr-only">Loading....</span>
					</div>
						:
						<div className="product-category">
							<select className="form-control mb-3" name="categories" id="categories" onChange={handleChangeSelected	}>
								<option>Select Category</option>
								{
									categories.map(category=>{
										return (
											<option value={category._id}>{category.name}</option>
										)
									})
								}
							</select>
							<button type="submit" className="btn btn-danger">Delete</button>
						</div>
				}
				
			</form>
		</div>
	)
}

export default DeleteCategory;
