import React, {Fragment,useState} from "react";

const RegisterForm = () =>{

	const[formData,setFormData] = useState({
		firstname : "",
		lastname : "",
		email : "",
		password : "",
		confirmPassword : ""
	})

	const {firstname,lastname,email,password,confirmPassword} = formData;

	const onChangeHandler = e =>{
		setFormData({...formData,[e.target.name]:e.target.value})
	}

	const handleRegister = e =>{
		e.preventDefault();
		if(password !== confirmPassword){
			alert("Passwords do not match")
		}else{
			fetch("https://bookacar.herokuapp.com/users/register",{
				method : "POST", headers: {
				"Content-Type" : "application/json",
				// "Accepts" : "application/json"
				},
				body : JSON.stringify(formData)
			})
			.then(data => data.json())
			.then(user => {
				console.log(user)
				setFormData({firstname : "",lastname : "",email : "",password : "",confirmPassword : ""
				})

				if(user.message){
					let element = document.getElementById("message")
					element.innerHTML = user.message
					element.classList.toggle("d-none")
					setTimeout(function(){ element.classList.toggle("d-none") }, 1500)
				}else{
					let element = document.getElementById("message")
					element.innerHTML = user.messageS
					element.classList.toggle("d-none")
					element.classList.remove("alert-danger")
					element.classList.add("alert-success")
					setTimeout(function(){ element.classList.toggle("d-none") }, 1500)
					window.location.href = "/login"
				}

				// if(user.messageS){
				// 	let element = document.getElementById("messagE")
				// 	element.innerHTML = user.messageS
				// 	element.classList.toggle("d-none")
				// 	setTimeout(function(){ element.classList.toggle("d-none") }, 1500)
				// }
			})
		}
	}

	return(
		<Fragment>
			<section className="login-register-form reg-margin">
				<div className="register-container">
					<h1>Register</h1>
				</div>
				<div className="register-form-container">
					<form onSubmit={ e => handleRegister(e)}>
						<div className="firstname-input-container">
							<input type="text" id="firstname" name="firstname" placeholder="First Name" value={firstname} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="lastname-input-container">
							<input type="text" id="lastname" name="lastname" placeholder="Last Name" value={lastname} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="email-input-container">
							<input type="email" id="email" name="email" placeholder="Email" value={email} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="password-input-container">
							<input type="password" id="password" name="password" placeholder="Password" value={password} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="confirm-password-input-container">
							<input type="password" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" value={confirmPassword} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="submit-input-container">
							<button type="submit" id="button-hover">Register</button>
						</div>
					</form>
				</div>
			</section>
		</Fragment>
	)
}

export default RegisterForm;
