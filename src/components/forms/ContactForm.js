import React,{Fragment, useState, useEffect} from "react";

const ContactForm = () =>{

	const [message,setMessage] = useState({
		name : null,
		email : null,
		subject : null,
		message : null
	})

	const handleOnChangeText = e =>{
		setMessage({...message, [e.target.name] : e.target.value});
	}
		const formData = new FormData();

	const handleSendMessage = e =>{
		e.preventDefault();
		formData.append('name',message.name);
		formData.append('email',message.email);
		formData.append('subject',message.subject);
		formData.append('message',message.message);

		console.log(formData);
		let token = localStorage.getItem("token");

		fetch("https://bookacar.herokuapp.com/messages/create",{
			method : "POST",
			body : JSON.stringify(message),
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then( data => data.json())
		.then( message => console.log(message));
	}

	console.log(message)

	return(
		<Fragment>
			<section className="login-register-form margin">
				<div className="contact-header">
					<h5>Get in touch with us!</h5>
				</div>
				<div className="contact-form-container">
					<form onSubmit={handleSendMessage}>
						<div className="contact-name-container">
							<input type="text" name="name" id="name" placeholder="Name" onChange={handleOnChangeText}/>
						</div>
						<div className="contact-email-container">
							<input type="email" name="email" id="email" placeholder="Email" onChange={handleOnChangeText}/>
						</div>
						<div className="contact-subject-container">
							<input type="text" name="subject" id="subject" placeholder="Subject" onChange={handleOnChangeText}/>
						</div>
						<div className="contact-message-container">
							<textarea name="message" id="message" cols="25" rows="5" placeholder="Message..." onChange={handleOnChangeText}></textarea>
						</div>
						<div className="contact-button-container">
							<button type="submit" id="button-hover">Send Message</button>
						</div>
					</form>
				</div>
			</section>
		</Fragment>
	)
}

export default ContactForm;