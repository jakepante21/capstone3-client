import React, {useState} from "react";

const RegisterForm = () =>{

	const[formData,setFormData] = useState({
		firstname : "",
		lastname : "",
		email : "",
		password : "",
		confirmPassword : ""
	})

	const {firstname,lastname,email,password,confirmPassword} = formData;

	const onChangeHandler = e =>{
		setFormData({...formData,[e.target.name]:e.target.value})
	}

	const handleRegister = e =>{
		e.preventDefault();
		if(password !== confirmPassword){
			alert("Passwords do not match")
		}else{
			fetch("http://localhost:3001/users/register",{
				method : "POST", headers: {
				"Content-Type" : "application/json",
				// "Accepts" : "application/json"
				},
				body : JSON.stringify(formData)
			})
			.then(data => data.json())
			.then(user => {
				console.log(user)
				setFormData({firstname : "",lastname : "",email : "",password : "",confirmPassword : ""
				})

				if(user.message){
					let element = document.getElementById("message")
					element.innerHTML = user.message
					element.classList.toggle("d-none")
					setTimeout(function(){ element.classList.toggle("d-none") }, 1500)
				}else{
					let element = document.getElementById("message")
					element.innerHTML = user.messageS
					element.classList.toggle("d-none")
					element.classList.remove("alert-danger")
					element.classList.add("alert-success")
					setTimeout(function(){ element.classList.toggle("d-none") }, 1500)
					window.location.href = "/login"
				}

				// if(user.messageS){
				// 	let element = document.getElementById("messagE")
				// 	element.innerHTML = user.messageS
				// 	element.classList.toggle("d-none")
				// 	setTimeout(function(){ element.classList.toggle("d-none") }, 1500)
				// }
			})
		}
	}

	return(
		<div className="container general-h">
			<div className="row">
				<div className="col-lg-6 mx-auto pt-4">
					<div id="message" className="alert alert-danger d-none" role="alert">
					</div>
					<h2 className="text-center text-light">Register</h2>
					<form onSubmit={ e => handleRegister(e)}>
						<div className="form-group">
							<label htmlFor="firstname" className="text-light">First Name</label>
							<input type="text" className="form-control" id="firstname" name="firstname" value={firstname} onChange={ e => {onChangeHandler(e)}}/>
							<h3>{firstname}</h3>
						</div>
						<div className="form-group">
							<label htmlFor="lastname" className="text-light">Last Name</label>
							<input type="text" className="form-control" id="lastname" name="lastname" value={lastname} onChange={ e => {onChangeHandler(e)}}/>
							<h3>{lastname}</h3>
						</div>
						<div className="form-group">
							<label htmlFor="email" className="text-light">Email</label>
							<input type="email" className="form-control" id="email" name="email" value={email} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="form-group">
							<label htmlFor="password" className="text-light">Password</label>
							<input type="password" className="form-control" id="password" name="password" value={password} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<div className="form-group">
							<label htmlFor="confirmPassword" className="text-light">Confirm Password</label>
							<input type="password" className="form-control" id="confirmPassword" name="confirmPassword" value={confirmPassword} onChange={ e => {onChangeHandler(e)}}/>
						</div>
						<button type="submit" className="btn btn-primary">Register</button>
					</form>	
				</div>
			</div>
		</div>
	)
}

export default RegisterForm;
