import React, {useState} from "react";


const EditCategory = ({categories,handleChangeCategoriesStatus,categoriesStatus}) =>{

	const [selectedCategory,setSelectedCategory] = useState({
		name : null,
		id : null
	})

	const handleChangeSelected = (e) => {
		let name = categories.find(category=>{
			return category._id === e.target.value
		})
		setSelectedCategory({
			id : e.target.value,
			name : name.name
		})
	}

	const handleChangeName = (e) => {
		setSelectedCategory({...selectedCategory,[e.target.name] : e.target.value})
	}

	const handleEditCategory = (e) => {
		e.preventDefault();
		fetch("https://bookacar.herokuapp.com/categories/"+selectedCategory.id,{
			method : "PUT",
			body : JSON.stringify({name : selectedCategory.name}),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			}
		})
		.then(data => { return data.json() })
		.then(category => handleChangeCategoriesStatus({
			lastUpdated : selectedCategory.id,
			status : "PASS!",
			isLoading : true
		}))
	}

	return(
		<div className="add-product-container mt-3">
			<form onSubmit={handleEditCategory}>
				<div className="add-room-title">
					<h1>Edit Category</h1>
				</div>
				{
					categoriesStatus.isLoading ? 
						<div className="spinner-border" role="status">
							<span className="sr-only">Loading....</span>
						</div>
						:
						<div className="product-category">
							<select className="form-control mb-3" name="products" id="products" onChange={handleChangeSelected}>
							<option disabled selected>Select Category</option>
							{
								categories.map(category=>{
									return(
										<option data-name={category.name} value={category._id}>{category.name}</option>
									)
								})
							}
							</select>
							<div className="product-name">
								<input type="text" placeholder="Category Name" name="name" id="name" value={selectedCategory.name} onChange={handleChangeName}/>
							</div>
							<div className="add-button">
								<button type="submit" className="btn btn-warning">Update</button>
							</div>
						</div>
				}
				
			</form>
		</div>
	)
}

export default EditCategory;
