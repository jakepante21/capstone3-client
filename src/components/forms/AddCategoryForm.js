import React,{useState} from "react";

const AddCategory = () =>{

	const [category,setCategory] = useState("");

	const handleCategoryChange = (e) =>{
		setCategory(e.target.value)
	}

	const handleAddCategory = (e) =>{
		e.preventDefault();
		let url = "https://bookacar.herokuapp.com/categories/create";
		let data = { name : category}
		let token = localStorage.getItem("token");
		fetch(url,{
			method : "POST",
			body : JSON.stringify(data),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : token
			}
		})
		.then(data => data.json())
		.then(category => {
			console.log(category)
		})
	}

	return(
		<div className="add-product-container mt-3">
			<form onSubmit={handleAddCategory}>
				<div className="add-room-title">
					<h1>Add Category</h1>
				</div>
				<div className="product-name">
					<input type="text" placeholder="Category Name" name="name" id="name" onChange={handleCategoryChange}/>
				</div>
				<div className="add-button">
					<button type="submit" className="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	)
}

export default AddCategory;
