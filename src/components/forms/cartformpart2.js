import React,{useState,useEffect,useReducer} from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import {DateRangeInput, DateSingleInput, Datepicker} from '@datepicker-react/styled';
// import { ThemeProvider } from "styled-components";

// const initialState = {
// 		startDate: null,
// 	  	endDate: null,
// 	  	focusedInput: null,
// 	}

// function reducer(state, action) {
//   	switch (action.type) {
//     	case 'focusChange':
//       		return {...state, focusedInput: action.payload}
//     	case 'dateChange':
//       		return action.payload
//    	 	default:
//       		throw new Error()
//   }
// }

const CartForm = ({products,cart,date,handleClearCart,handleRemoveItem,total}) =>{

	const [order,setOrder] = useState([]);
	const [finalOrder,setFinalOrder] = useState([]);
	// const [startDate, setStartDate] = useState(new Date());
	// const [endDate, setEndDate] = useState(new Date());

	// const [state, dispatch] = useReducer(reducer, initialState)

	const changeDate = (e) =>{
		setOrder({...order,[e.target.name] : e.target.value})
	}

	const handleCheckoutt = () => {
		
		
	}

	useEffect(()=>{
		let orders = cart.map(item => {
			if(item._id){
				return {
					id : item._id,
					days : 1
				}
			}
		})

		setFinalOrder(orders)

		// setOrder({orders : finalOrder , startDate : date.startDate , endDate : date.endDate})
		// console.log(date.startDate)

	},[])

	useEffect(()=>{
		setOrder({orders : finalOrder , startDate : date.startDate , endDate : date.endDate})
	},[finalOrder])

	const test = () =>{
		console.log(date.startDate)
		setOrder({orders : finalOrder , startDate : date.startDate , endDate : date.endDate})
	}

	// console.log(date)
	console.log(finalOrder)
	console.log(order)

	// console.log(state.startDate)
	const handleCheckoutOne = () =>{

		// setOrder({...order,startDate : state.startDate,endDate : state.endDate})

		fetch("http://localhost:3001/transactions",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify({order,paymentMode : "Over the counter"})
		})
		.then(data => data.json())
		.then(result => 
			console.log(result)
			// window.location.href ="/transactions"
		)
	}

	const handleCheckoutTwo = () =>{

		// setOrder({...order,startDate : state.startDate,endDate : state.endDate})

		fetch("http://localhost:3001/transactions",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify(order)
		})
		.then(data => data.json())
		.then(result => {
			fetch("http://localhost:3001/transactions/stripe",{
				method : "POST",
				headers : {
					"Content-Type" : "application/json",
					"Authorization" : localStorage.getItem("token")
				},
				body : JSON.stringify({userId : result.userId,total : result.total})
			})
			.then(data => data.json())
			.then(result => {
				window.open(result.receipt_url)
				
			})
		}
			// console.log(result)
			// window.location.href ="/transactions"
		)
		// console.log({order,paymentMode : "STripe"})
	}

	return (
		<div className="container">
			<div className="jumbotron">
				<h1 className="text-center">Your Cart</h1>
			</div>
			<div className="row">
				<form>
					
				</form>
			</div>
			<div className="row">
			
				<table className="table table-striped">
					<thead>
						<tr>
							<th scope="col" className="text-light">Product Name</th>
							<th scope="col" className="text-light">Price</th>
							<th scope="col" className="text-light">Action</th>
						</tr>
					</thead>
					<tbody>
						
						{	
							cart.map(item=>{
								
								return(
									<tr>
										<td className="text-light">{item.name}</td>
										<td className="text-light"><span>&#8369;{item.price}</span></td>
										<td>
											<button className="btn btn-outline-danger" onClick={ () => {handleRemoveItem(item)}}>Remove from the cart</button>
										</td>
									</tr>
								)
								
								
							})
						}
						
						<tr>
							<td colspan="2" className="text-light">Total</td>
							<td className="text-light">{total}</td>
						</tr>

					</tbody>
					<tr>
						<td colspan="2">
							<button className="btn btn-warning" onClick={ () =>{handleClearCart([])}}>Clear Cart</button>
						</td>
						<td>
							<button className="btn btn-warning" onClick={handleCheckoutTwo}>Pay With Stripe</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	)
}

export default CartForm;

/*

categories
	single room
		ceasar's room
		
		room # 002
			w/ different amenities
	double room

	family room


type 1 = Amenities [ computer w/ internet, fridge ,satellite tv]
type 2 = Amenities [ electric kettle, computer w/ internet, fridge , microwave , satellite tv]
type 3 = Amenities [ electric kettle, computer w/ internet, fridge , microwave , satellite tv and washing machine, balcony]

*/