import React,{Fragment,useState,useEffect,useReducer} from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import {DateRangeInput, DateSingleInput, Datepicker} from '@datepicker-react/styled';
// import { ThemeProvider } from "styled-components";

// const initialState = {
// 		startDate: null,
// 	  	endDate: null,
// 	  	focusedInput: null,
// 	}

// function reducer(state, action) {
//   	switch (action.type) {
//     	case 'focusChange':
//       		return {...state, focusedInput: action.payload}
//     	case 'dateChange':
//       		return action.payload
//    	 	default:
//       		throw new Error()
//   }
// }

const CartForm = ({products,cart,date,handleClearCart,handleRemoveItem}) =>{

	const [order,setOrder] = useState([]);
	const [finalOrder,setFinalOrder] = useState([]);
	const [total,setTotal] = useState(0);

	let days = localStorage.getItem("days");
	let daysInt = parseInt(days);
	let pickedDate = localStorage.getItem("pickedDate");
	let finalDate = JSON.parse(pickedDate);
	// const [startDate, setStartDate] = useState(new Date());
	// const [endDate, setEndDate] = useState(new Date());

	// const [state, dispatch] = useReducer(reducer, initialState)

	let localCart;

    	if(localStorage.getItem("cart") !== null){
	        const newCart = localStorage.getItem("cart");
	        localCart = JSON.parse(newCart);
	        console.log(localCart)
	    }else{
	        // let localCart =[];
	        console.log("what")
	    }

	const changeDate = (e) =>{
		setOrder({...order,[e.target.name] : e.target.value})
	}

	const handleCheckoutt = () => {
		
		
	}

	useEffect(()=>{
		let orders = localCart.map(item => {
			if(item._id){
				return {
					id : item._id,
					roomId : item.roomId,
					number : item.roomNumber,
					type : item.roomType,
					days : daysInt
				}
			}
		})

		setFinalOrder(orders)
		// console.log(date)

		// setOrder({orders : finalOrder , startDate : date.startDate , endDate : date.endDate})
		// console.log(date.startDate)

	},[])

	useEffect(()=>{
		console.log(typeof(date.startDate))
		setOrder({orders : finalOrder , startDate : finalDate.startDate , endDate : finalDate.endDate})
	},[finalOrder])

	const test = () =>{
		console.log(date.startDate)
		setOrder({orders : finalOrder , startDate : date.startDate , endDate : date.endDate})
	}

	// console.log(date)
	console.log(finalOrder)
	console.log(order)

	// console.log(state.startDate)
	const handleCheckoutOne = () =>{

		// setOrder({...order,startDate : state.startDate,endDate : state.endDate})

		fetch("https://bookacar.herokuapp.com/transactions",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify({order,paymentMode : "Over the counter"})
		})
		.then(data => data.json())
		.then(result => 
			console.log(result)
			// window.location.href ="/transactions"
		)
	}

	const handleCheckoutTwo = () =>{

		// setOrder({...order,startDate : state.startDate,endDate : state.endDate})

		fetch("https://bookacar.herokuapp.com/transactions",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify(order)
		})
		.then(data => data.json())
		.then(result => {
			console.log(result);
			fetch("https://bookacar.herokuapp.com/transactions/stripe",{
				method : "POST",
				headers : {
					"Content-Type" : "application/json",
					"Authorization" : localStorage.getItem("token")
				},
				body : JSON.stringify({userId : result.userId,total : result.total})
			})
			.then(data => data.json())
			.then(result => {
				window.open(result.receipt_url);
				window.location.href="/cart"
			})

		}
			// console.log(result)
			// window.location.href ="/transactions"
		)

		localStorage.removeItem("cart");
		// console.log({order,paymentMode : "STripe"})
	}

	console.log(cart);

	useEffect( () => {
		let subTotal = 0;
		localCart.forEach(ohcrap => {
			let subTotal2 = ohcrap.price * days;
			subTotal += subTotal2;
		})
		setTotal(subTotal);
	},[])

	return (
		<Fragment>
			<div className="cart-form-header">
				<h1>Room/s to Book</h1>
			</div>
			<section>
				<table className="table table-responsive-sm">
					<thead>
						<tr>
							<th scope="col">Room Name</th>
							<th scope="col">No. of Days</th>
							<th scope="col">Price</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						
						{	
							localCart.map(item=>{
								
								return(
									<tr>
										<td>{item.name}</td>
										<td>{daysInt}</td>
										<td><span>&#8369;{item.price}</span></td>
										<td>
											<button className="btn btn-outline-danger" onClick={ () => {handleRemoveItem(item)}}>Remove from the cart</button>
										</td>
									</tr>
								)
								
								
							})
						}
						
						<tr>
							<td colspan="3">Total</td>
							<td>{total}</td>
						</tr>

					</tbody>
					<tfoot>
						<tr>
							<td colspan="3">
								<button className="btn btn-warning" onClick={ () =>{handleClearCart([])}}>Clear Cart</button>
							</td>
							<td>
								<button className="btn btn-warning" onClick={handleCheckoutTwo}>Pay With Stripe</button>
							</td>
						</tr>
					</tfoot>
				</table>
			</section>
		</Fragment>
	)
}

export default CartForm;

/*

categories
	single room
		ceasar's room
		
		room # 002
			w/ different amenities
	double room

	family room


type 1 = Amenities [ computer w/ internet, fridge ,satellite tv]
type 2 = Amenities [ electric kettle, computer w/ internet, fridge , microwave , satellite tv]
type 3 = Amenities [ electric kettle, computer w/ internet, fridge , microwave , satellite tv and washing machine, balcony]

*/