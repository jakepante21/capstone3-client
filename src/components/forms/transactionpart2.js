import React,{useState,Fragment}   from "react";

const Transaction = ({transactions,rooms}) => {

	const [bookedStatus,setBookedStatus] = useState({
		status : "Booked"
	});

	const [availStatus,setAvailStatus] = useState({
		status : "Available"
	});

	const [completedStatus,setCompletedStatus] = useState({
		status : "Completed"
	})
	const [acceptedStatus,setAcceptedStatus] = useState({
		status : "Accepted"
	})
	const [rejectedStatus,setRejectedStatus] = useState({
		status : "Rejected"
	})

	const [dateReserved,setDateReserved] = useState();

	const [newUnits,setNewUnits] = useState([]);

	const [newDates,setNewDates] = useState([]);

	const [currTransaction,setCurrTransaction] = useState();

	const [unitChange,setUnitChange] = useState();

	let a = JSON.parse(localStorage.getItem("dates"));
	let b = JSON.parse(localStorage.getItem("rooms"));

	const rejectTrans = (id) =>{
		fetch("http://localhost:3001/transactions/" + id,{
			method : "PUT",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify(rejectedStatus)
		})
		.then(data => data.json())
		.then(result => window.location.href="/transactions")
	}

	const transacStatus = (id) =>{
		fetch("http://localhost:3001/transactions/" + id,{
			method : "PUT",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify(completedStatus)
		})
		.then(data => data.json())
		.then(result => {
			window.location.href="/transactions"
		})
	}

	const getBetweenDates = (start,end) => {
		let arrDates = [];
		let sDate = new Date(start);
		let eDate = new Date(end);
		
		while( sDate <= eDate){
			arrDates.push(new Date(sDate))
			sDate.setDate(sDate.getDate() + 1);
		}
		
		// console.log(arrDates)
		return arrDates;
	} 

	const filterUnit = (transCode) =>{
		let newTransaction = transactions.find(transaction => {
			return transaction.transactionCode === transCode
		})

		setCurrTransaction(newTransaction);

		let newUnit = rooms.filter(room=>{
			let x = newTransaction.products.find(product=>{
				return room.productId === product.productId
			})
			// console.log(x)
			return x
		})

		let newUnitAgain = newUnit.filter(unit=>{
			return unit.status === "Available"
		})

		// console.log(newUnit)

		setNewUnits(newUnitAgain)
		// console.log(newUnit)
		// console.log(newTransaction)
		// console.log(newTransaction.startDate)
		let newArrayDate = getBetweenDates(newTransaction.startDate,newTransaction.endDate)
		let newNewArrayDate = newArrayDate.map(a => {
			return {
				date : a
			}
		})
		setNewDates(newNewArrayDate);
		// console.log(new)

	}

	console.log(newDates)

	// console.log(newUnits)

	const assignUnit = (e) =>{

		let user = localStorage.getItem("user");
		let id = JSON.parse(user);

		fetch("http://localhost:3001/rooms/" + e ,{
			method : "PUT",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify(bookedStatus)
		})
		.then(data => data.json())
		.then(result => {
				fetch("http://localhost:3001/rentedrooms/create",{
					method : "POST",
					headers : {
						"Content-Type" : "application/json",
						"Authorization" : localStorage.getItem("token")
					},
					body : JSON.stringify({
						productId : result.productId,
						type : result.type,
						roomNumber : result.roomNumber,
						status : result.status,
						startDate : currTransaction.startDate,
						endDate : currTransaction.endDate,
						reservationCode : currTransaction.transactionCode,
						userId : id.id
					})
				})
				.then(data => data.json())
				.then(result => {
					fetch("http://localhost:3001/dates/create",{
						method : "POST",
						headers : {
							"Content-Type" : "application/json",
							"Authorization" : localStorage.getItem("token")
						},
						body : JSON.stringify({
							reservationCode : currTransaction.transactionCode,
							roomNumber : result.roomNumber,
							productId : result.productId,
							dates : newDates
						})
					})
					.then(data => data.json())
					.then(result => {
						fetch("http://localhost:3001/transactions/" + currTransaction._id,{
							method : "PUT",
							headers : {
								"Content-Type" : "application/json",
								"Authorization" : localStorage.getItem("token")
							},
							body : JSON.stringify({status : "Accepted"})
						})
						.then(data => data.json())
						.then(result => window.location.href="/transactions")
					})
				})
		})


	}

	let user = localStorage.getItem("user");
	let id = JSON.parse(user);

	// const test = () =>{
	// 	let a = localStorage.getItem("user");
	// 	let b = JSON.parse(a)
	// 	console.log(b.id)
	// }


	return(
		<Fragment>
			<div className="container">
				<div className="transaction-header-container">
					<h1>Reservations</h1>
				</div>
				
				<div id="accordion" className="mt-5">
				  	{
				  		transactions.map(transaction=>{
				  			return(
				  				<div class="card">
								    <div class="card-header bg-dark" id="headingOne">
								      <h5 class="mb-0">
								        <button class="btn btn-link text-light" data-toggle="collapse" data-target={"#a" + transaction.transactionCode} aria-expanded="true" aria-controls="collapseOne">
								          Reservation Code : {transaction.transactionCode}
								        </button>
								      </h5>
								    </div>
								    <div id={"a" + transaction.transactionCode} class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
								      <div class="card-body">
								        <table class="table table-hover table-striped table-dark">
											<tr>
												<td>Reservation Code:</td>
												<td>{transaction.transactionCode}</td>
											</tr>
											<tr>
												<td>Customer:</td>
												<td>{transaction.userId}</td>
											</tr>
											<tr>
												<td>Payment Mode:</td>
												<td>{transaction.paymentMode}</td>
											</tr>
											<tr>
												<td>Status</td>
												<td>{transaction.status}</td>
											</tr>
											<tr>
												<td>Start Date</td>
												<td>{(new Date(transaction.startDate)).toString()}</td>
											</tr>
											<tr>
												<td>End Date</td>
												<td>{(new Date(transaction.endDate)).toString()}</td>
											</tr>
											{
												

												id.role === "admin" ?
													transaction.status === "Pending" ?
													<tr>
														<td>Action</td>
														<td>
															<form>
																<button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable" onClick={()=>{filterUnit(transaction.transactionCode)}}>Accept</button>
															</form>
														</td>
														<td>
															<form>
																<button className="btn btn-danger" onClick={()=>{rejectTrans(transaction._id)}}>Reject</button>
															</form>
														</td>
													</tr>
													:
														transaction.status === "Rejected" ?
															<tr>
																<td>Action</td>
																<td>
																	Rejected
																</td>
															</tr>
														:
														<tr>
															<td>Action</td>
															<td>
																<td>
																	Completed
																</td>
															</td>
														</tr>
												:
												""
											}
											
										</table>
										<table class="table table-responsive table-hover table-striped table-dark">
											<tbody>
												<tr>
													<td>Unit Name:</td>
													<td>Room Number:</td>
													<td>No. of Days:</td>
													<td>Subtotal:</td>
												</tr>
												{
													transaction.products.map(product=>{
														return(
															<tr>
																<td>{product.name}</td>
																<td>{product.roomNumber}</td>
																<td>{product.days}</td>
																<td>{product.subtotal}</td>
															</tr>
														)
													})
												}
												
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3">Total:</td>
													<td>&#8369;{transaction.total}</td>
												</tr>
											</tfoot>
										</table>
								      </div>
								    </div>
								  </div>
				  			)
				  		})
				  	}
				  
				
				</div>
			</div>
				<div className="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
				  	<div className="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
				    	<div className="modal-content jake-modal-container">
					      	<div className="modal-header bg-dark">
					        	<h5 className="modal-title text-light" id="exampleModalScrollableTitle">Set Unit</h5>
					        	<button type="button" className="close" data-dismiss="modal" aria-label="Close">
					          	<span aria-hidden="true" className="text-light">&times;</span>
					        	</button>
					      	</div>
					      	<div className="modal-body">
					      		<div>
	        						<h2 className="text-center">Available Units</h2>
	        					</div>
	        					<div className="container">
					        	{
					        		newUnits.map(room=>{
					        			return (

					        					<div className="row">
					        						<div className="col-12 col-md-2 m-2">
					        							<h5>{room.roomNumber}</h5>
					        						</div>
					        						<div className="col-12 col-md-4 m-2">
					        							<button className="btn btn-primary" onClick={ () => {assignUnit(room._id)}}>Assign Unit</button>
					        						</div>
					        					</div>

					        			)
					        		})
					        	}
					        	</div>
					      	</div>
					      	<div className="modal-footer">
					        	<button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
					        	<button type="button" className="btn btn-primary">Save changes</button>
					      	</div>
				    	</div>
				  	</div>
				</div>
		</Fragment>
	)
}




export default Transaction;