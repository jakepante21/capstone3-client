import React, {Fragment,useState} from "react";


const LoginForm = () =>{

	const [userData,setUserData] = useState([])

	const handleInputChange = (e) =>{
		setUserData({...userData,[e.target.name] : e.target.value})
	}

	const handleSubmit = (e) =>{
		e.preventDefault();
		fetch("https://bookacar.herokuapp.com/users/login",{
			method : "POST",
			body : JSON.stringify(userData),
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then(data => data.json())
		.then(user => {
			if(user.token){
				alert(user.message)
				localStorage.setItem("user",JSON.stringify(user.user));
				localStorage.setItem("token","Bearer " + user.token);
				window.location.href = "/"
			}
		})

	}



	return(
		<Fragment>
			<section className="login-register-form">
				<div className="login-container">
					<h1>Login</h1>
				</div>
				<div>
					<form onSubmit={handleSubmit}>
						<div className="email-input-container">
							<input type="email" id="email" name="email" placeholder="Email" onChange={handleInputChange}/>
						</div>
						<div className="password-input-container">
							<input type="password" id="password" name="password" placeholder="Password" onChange={handleInputChange}/>
						</div>
						<div className="submit-input-container">
							<button id="button-hover">Login</button>
						</div>
					</form>
				</div>
			</section>
		</Fragment>
	)
}

export default LoginForm;
