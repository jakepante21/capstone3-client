import React,{useState} from "react";


const AddProduct = ({categories}) =>{

	const [product, setProduct] = useState({
		name: null,
		categoryId : null,
		price: null,
		description: null,
		image: null,
		noOfBedRooms : null,
		maxPerson : null
	});

	const onChangeText = e => {
		console.log(e.target.value)
		setProduct({...product, [e.target.name] : e.target.value})

		// console.log(e.target.name)

		let peopleCount = categories.find(category =>{
			if(e.target.value === category._id){
				return category.name;
			}
		})

		if(peopleCount){
			if(peopleCount.name === "Single Room"){
				setProduct({...product,[e.target.name] : e.target.value, noOfBedRooms : 1 , maxPerson : 2});
			}else if(peopleCount.name === "Double Room"){
				setProduct({...product,[e.target.name] : e.target.value, noOfBedRooms : 2 , maxPerson : 4});
			}else{
				setProduct({...product,[e.target.name] : e.target.value, noOfBedRooms : 5 , maxPerson : 10});
			}
		}
	}

	const onChangeCategory = e =>{
		// setProduct({...product, [e.target.name] : e.target.value})

		
	}

	const handleChangeFile = e => {
		console.log(e.target.files[0])
		setProduct({
		...product,
		image: e.target.files[0]
		})
	}

	const handleAddProduct = e => {
		e.preventDefault()
		const formData = new FormData();
		formData.append('name', product.name)
		formData.append('price', product.price)
		formData.append('categoryId', product.categoryId)
		formData.append('description', product.description)
		formData.append('image', product.image)
		formData.append('noOfBedRooms', product.noOfBedRooms)
		formData.append('maxPerson', product.maxPerson)

		let token = localStorage.getItem('token');

		fetch("https://bookacar.herokuapp.com/products/create", {
		method : "POST", 
		body : formData, 
		headers : { 
		"Authorization" : token
		} 
		})
		.then( data => data.json())
		.then( result => {
			console.log(result);
			window.location.href="/admin-panel"
		})


	}


	return(
		<div className="add-product-container mt-3">
			<form enctype="multipart/form-data" onSubmit={handleAddProduct}>
				<div className="add-room-title">
					<h1>Add Room</h1>
				</div>
				<div className="product-name">
					<input type="text" placeholder="Product Name" name="name" id="name" onChange= {onChangeText}/>
				</div>
				<div className="product-price">
					<input type="number" placeholder="Product Price" name="price" id="price" onChange= {onChangeText}/>
				</div>
				<div className="product-image">
					<input type="file" name="image" id="image" onChange= {handleChangeFile}/>
				</div>	
				<div className="product-category">
					<select name="categoryId" id="category" onChange= {onChangeText}>
						<option disabled selected>Select Category</option>
						{
							categories.map(category=>{
								return (
									<option value={category._id}>{category.name}</option>
								)
							})
						}
					</select>
				</div>
				<div className="product-description">
					<textarea name="description" id="description" placeholder="Description" cols="30" rows="8" onChange= {onChangeText}></textarea>
				</div>
				<div className="add-button">
					<button type="submit" className="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	)
}

export default AddProduct;
