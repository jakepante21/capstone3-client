import React,{useState} from "react";


const EditProduct = ({categories,products,handleChangeCategoriesStatus,productsStatus}) =>{

	const [productss,setProducts] = useState({
		id : null,
		name: null,
		categoryId : null,
		price: null,
		description: null,
		image: null
	})

	const handleChangeSelected = (e) => {
		let productsss = products.find(product =>{
			return product._id === e.target.value
		})

		let currCategory = categories.find(category => {
			return category._id === productsss.categoryId;
		})
		// console.log(productsss)
		setProducts({...productsss, categoryName : currCategory.name})
	}

	const onChangeText = e => {
		console.log(e.target.value)
		setProducts({...productss, [e.target.name] : e.target.value})
	}

	const handleChangeFile = e => {
		console.log(e.target.files[0])
		setProducts({
		...productss,
		image: e.target.files[0]
		})
	}

    const handleEditProduct = (e) =>{
    	e.preventDefault()

		const formData = new FormData();
		formData.append('name', productss.name)
		formData.append('price', productss.price)
		formData.append('categoryId', productss.categoryId)
		formData.append('description', productss.description)
		if(typeof productss.image === "object"){
			formData.append('image', productss.image)
		}


		let token = localStorage.getItem('token');


		fetch("https://bookacar.herokuapp.com/products/"+productss._id, {
		method : "PUT", 
		body : formData, 
		headers : { 
				"Authorization" : token
			} 
		})
		.then( data => data.json())
		.then( result => console.log(result))
	}

	return(
		<div className="add-product-container mt-3">
			<form enctype="multipart/form-data" onSubmit={handleEditProduct}>
				<div className="add-room-title">
					<h1>Edit Room</h1>
				</div>
				<div className="product-category">
					<select name="products" id="products" onChange={handleChangeSelected}>
					{
						products.map(product=>{
							return(
								<option value={product._id}>{product.name}</option>
							)
						})
					}
				</select>
				</div>
				{
					typeof productss.image === "string" ? 
					<img src={"https://bookacar.herokuapp.com" + productss.image} style={{width : 150}} className="mb-2"/>
					:
					""
				}
				<div className="product-name">
					<input type="text" placeholder="Product Name" name="name" id="name" value={productss.name} onChange={onChangeText}/>
				</div>
				<div className="product-price">
					<input type="number" placeholder="Product Price" name="price" id="price" value={productss.price} onChange={onChangeText}/>
				</div>
				<div className="product-image">
					<input type="file" name="image" id="image" onChange={handleChangeFile}/>
				</div>	
				<div className="product-category">
					<select name="categoryId" id="category" onChange={onChangeText}>
						{
							categories.map(category=>{
								return(
									category._id === productss.categoryId ?
									<option value={category._id} selected>{category.name}</option>
									:
									<option value={category._id}>{category.name}</option>
								)
							})
						}
					</select>
				</div>
				<div className="product-description">
					<textarea name="description" id="description" placeholder="Description" cols="30" rows="8" value={productss.description} onChange={onChangeText}></textarea>
				</div>
				<div className="add-button">
					<button type="submit" className="btn btn-warning">Update</button>
				</div>
			</form>
		</div>
	)
}

export default EditProduct;
