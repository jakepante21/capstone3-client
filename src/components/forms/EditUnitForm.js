import React,{Fragment,useState,useEffect} from "react";

const EditUnit = ({rooms}) =>{

	const [unitStatus,setUnitStatus] = useState();
	const [selectedUnit,setSelectedUnit] = useState({
		id : null,
		roomNumber : null
	})

	const changeSelectedUnit = (e) =>{
		let number = rooms.find(room =>{
			return room._id === e.target.value;
		})
		setSelectedUnit({id : e.target.value,roomNumber : number})
	}

	const changeSelectedStatus = (e) =>{
		setUnitStatus(e.target.value);
	}

	const updateSelectedUnit = (e) =>{
		e.preventDefault();

		fetch("https://bookacar.herokuapp.com/rooms/" + selectedUnit.id, {
			method : "PUT",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify({status : unitStatus})
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}	

	return(
		<Fragment>
			<div className="add-product-container mt-3">
				<form className="edit-unit" onSubmit={updateSelectedUnit}>
					<div className="add-room-title">
						<h1>Edit Unit</h1>
					</div>
					<div className="product-category">
						<select name="units" id="units" onChange={changeSelectedUnit}>
						{
							rooms.map(room =>{
								return(
									<option data-name={room.roomNumber} value={room._id}>{room.roomNumber}</option>
								)
							})
						}
						</select>
					</div>
					<div className="product-category">
						<select name="unitStat" id="unitStat" onChange={changeSelectedStatus}>
							<option value="Available">Available</option>
							<option value="Unavailable">Unavailable</option>
							<option value="UnderMaintenance">Under Maintenance</option>
						</select>
					</div>
					<div className="add-button">
						<button type="submit" className="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</Fragment>
	)
}

export default EditUnit;