import React, {useState} from "react";


const LoginForm = () =>{

	const [userData,setUserData] = useState([])

	const handleInputChange = (e) =>{
		setUserData({...userData,[e.target.name] : e.target.value})
	}

	const handleSubmit = (e) =>{
		e.preventDefault();
		fetch("http://localhost:3001/users/login",{
			method : "POST",
			body : JSON.stringify(userData),
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then(data => data.json())
		.then(user => {
			if(user.token){
				alert(user.message)
				localStorage.setItem("user",JSON.stringify(user.user));
				localStorage.setItem("token","Bearer " + user.token);
				window.location.href = "/"
			}
		})

	}



	return(
		<div className="container general-h">
			<div className="row">
				<div className="col-12 col-lg-8 mx-auto">
					<div className="jumbotron">
						<h1 className="text-center">Login</h1>
					</div>
					<form onSubmit={handleSubmit}>
						<div className="form-group">
							<input type="text" name="email" id="username" className="form-control" placeholder="Username" onChange={handleInputChange}/>
						</div>
						<div className="form-group">
							<input type="password" name="password" id="password" className="form-control" placeholder="Password" onChange={handleInputChange}/>
						</div>
						<button className="btn btn-primary w-100">Login</button>
					</form>
				</div>
			</div>
		</div>
	)
}

export default LoginForm;
