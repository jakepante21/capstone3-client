import React,{useState,Fragment} from "react";

const DeleteProduct = ({products}) =>{

	const [selected,setSelected] = useState({
		productId : null
	})

	const changeOption = (e) =>{
		setSelected({[e.target.name] : e.target.value})
		// console.log(selected)
	}

	const deleteProduct = (e) =>{
		e.preventDefault();
		fetch("https://bookacar.herokuapp.com/products/" + selected.productId , {
			method : "DELETE",
			headers : {
				"Authorization" : localStorage.getItem("token")
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))

	}

	return(
		<Fragment>
			<div className="add-product-container mt-3">
				<form onSubmit={deleteProduct}>
					<div className="add-room-title">
						<h1>Delete Room</h1>
					</div>
					<div className="product-category">
						<select name="productId" id="productId" onChange={changeOption}>
							{
								products.map(product=>{
									return(
										<option value={product._id}>{product.name}</option>
									)
								})
							}
						</select>
					</div>
					<div className="add-button">
						<button className="btn btn-danger">Delete Product</button>
					</div>
				</form>
			</div>
		</Fragment>
	)
}

export default DeleteProduct;