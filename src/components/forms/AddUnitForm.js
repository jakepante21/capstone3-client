import React,{useState,Fragment} from "react";

const AddUnit = ({products}) =>{

	const [unit,setUnit] = useState([]);

	const textChange = (e) =>{
		setUnit({...unit,[e.target.name] : e.target.value})
	}

	const productChange = (e) =>{
		setUnit({...unit,[e.target.name] : e.target.value})
	}

	const typeChange = (e) =>{
		setUnit({...unit,[e.target.name] : e.target.value})
	}

	const addUnit = (e) =>{
		e.preventDefault();

		const formData = new FormData();
		// formData.append("units",1);

		fetch("https://bookacar.herokuapp.com/rooms/create",{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem("token")
			},
			body : JSON.stringify(unit)
		})
		.then(data => data.json())
		.then(result => {
			console.log(result);
			let newProduct = products.find(product =>{
				return product._id === result.productId;
			});
			let newUnit = newProduct.units + 1;
			formData.append("units",newUnit);
			fetch("https://bookacar.herokuapp.com/products/" + result.productId , {
				method : "PUT",
				headers : {
					"Authorization" : localStorage.getItem("token")
				},
				body : formData
			})
			.then(data => data.json())
			.then(result => {
				console.log(result);
				window.location.href = "/admin-panel";
			})
		})
	}

	return(
		<Fragment>
			<div className="add-product-container mt-3">
				<form onSubmit={addUnit}>
					<div className="add-room-title">
						<h1>Add Room Unit</h1>
					</div>
					<div className="product-name">
						<input type="text" className="form-control" placeholder="Room Number" name="roomNumber" id="roomNumber" onChange={textChange}/>
					</div>
					<div className="product-category">
						<select className="form-control" name="productId" id="productId" onChange={productChange}>
							<option disabled selected>Select Room Name</option>
							{
								products.map(product=>{
									return(
										<option value={product._id}>{product.name}</option>
									)
								})
							}
						</select>
					</div>
					<div className="product-category">
						<select className="form-control" name="type" id="type" onChange={typeChange}>
							<option disabled selected>Select Room Type</option>
							<option value="Standard">Standard</option>
							<option value="Deluxe">Deluxe</option>
							<option value="Suite">Suite</option>
						</select>
					</div>
					<div className="add-button">
						<button type="submit" className="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</Fragment>
	)
}

export default AddUnit;