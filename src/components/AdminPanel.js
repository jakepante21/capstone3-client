import React,{Fragment} from "react";
import ProductPanel from "./SubAdminPanel/ProductPanel";
import CategoryPanel from "./SubAdminPanel/CategoryPanel";
import UnitPanel from "./SubAdminPanel/UnitPanel";

const AdminPanel = ({categories,products,handleChangeCategoriesStatus,categoriesStatus,rooms}) =>{
	return(
		<Fragment>
			<section className="admin-panel-container">
				<div className="admin-panel-header">
					<h4>Welcome to Admin Panel</h4>
					<h5>Select what you want to manipulate.</h5>
				</div>
				<div className="admin-panel-body">
					<div className="product-button">
						<button data-toggle="collapse" data-target="#productCrud" aria-expanded="false" aria-controls="collapseExample">Room</button>
					</div>
					<div className="category-button">
						<button data-toggle="collapse" data-target="#categoryCrud" aria-expanded="false" aria-controls="collapseExample">Category</button>
					</div>
					<div className="unit-button">
						<button data-toggle="collapse" data-target="#unitCrud" aria-expanded="false" aria-controls="collapseExample">Unit</button>
					</div>
				</div>
				<div className="admin-panel-collapse">
					<div id="parentCrud">
						<div className="collapse" id="productCrud" data-parent="#parentCrud">
							<div className="product-container">
							    <ProductPanel categories={categories} products={products}/>
							</div>
						</div>
						<div className="collapse" id="categoryCrud" data-parent="#parentCrud">
							<div className="category-container">
							    <CategoryPanel categories = {categories} handleChangeCategoriesStatus = {handleChangeCategoriesStatus} categoriesStatus = {categoriesStatus}/>
							</div>
						</div>
						<div className="collapse" id="unitCrud" data-parent="#parentCrud">
							<div className="room-container">
							    <UnitPanel products={products} rooms={rooms}/>
							</div>
						</div>
					</div>
				</div>
			</section>
		</Fragment>
	)
}

export default AdminPanel;