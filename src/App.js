import React, {useEffect,useState} from 'react';
import Navbar from "./components/layouts/Nav";
import Footer from "./components/layouts/Footer";
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import LoginForm from "./components/forms/LoginForm";
import RegisterForm from "./components/forms/RegisterForm";
import AdminPanel from "./components/AdminPanel";
import Cart from "./components/Cart";
import Transaction from "./components/Transaction";
import Contact from "./components/Contact";


function App() {

    const [rooms,setRooms] = useState([]);

	const [products,setProducts] = useState([]);

	const [categories,setCategories] = useState([]);

    const [transactions,setTransactions] = useState([]);

    const [cart,setCart] = useState([]);

    const [reservedDates,setReservedDates] = useState([]);

    const [date,setDate] = useState([]);

    const [total,setTotal] = useState(0);

    let localCart;
    let varStorage = [];

    useEffect( () => {
        if(localStorage.getItem("cart") !== null){
            const newCart = localStorage.getItem("cart");
            localCart = JSON.parse(newCart);
            // extra += 1;
            // console.log(localCart);
        }
        // console.log(varStorage)
        if(localStorage.getItem("user") !== null){
            let keys = Object.keys(localStorage);
            var i = keys.length;

            for(var a = 0; a<=i-1;a++){
                varStorage.push({ [keys[a]] : localStorage.getItem(keys[a])})
            }
            // console.log(varStorage)
        }

        // if(varStorage.length > 0){
            
                // for(var a = 0; varStorage.length-1; a++){
                //     let index = Object.keys(varStorage[a]);
                //     localStorage.setItem(index[0],varStorage[a]);
                // }  

        //         console.log("WHY?")
        //         alert("why?");
            
        // }

        // let keys = Object.keys(localStorage);
        // var i = keys.length;

        // for(var a = 0; a<=i-1;a++){
        //     varStorage.push({ [keys[a]] : localStorage.getItem(keys[a])})
        // }

        // let index = Object.keys(varStorage[1]);
        // console.log(index[0])

        // console.log(localCart);
    },[])

    const storFunction = () =>{
        // let keys = Object.keys(localStorage);
        // var i = keys.length;

        // for(var a = 0; a<=i-1;a++){
        //     varStorage.push({ [keys[a]] : localStorage.getItem(keys[a])})
        // }
        // console.log(varStorage)

    }

    window.onbeforeunload = () =>{
        // storFunction();
        // let keys = Object.keys(localStorage);
        // var i = keys.length;

        // for(var a = 0; a<=i-1;a++){
        //     varStorage.push({ [keys[a]] : localStorage.getItem(keys[a])})
        // }
        // console.log(varStorage)
        // alert("WHAY?");

        if(varStorage.length > 1){
            window.onunload = () => {
                localStorage.clear();
            }
        }
    }
    // console.log(varStorage)

    useEffect( () =>{
        if(varStorage.length > 1){
            // console.log("true");
            if(localStorage.getItem("user") === null){
                // console.log("true");
                for(var a = 0; varStorage.length-1; a++){
                    let index = Object.keys(varStorage[a]);
                    localStorage.setItem(index[0],varStorage[a]);
                }
                varStorage = [];
            }else{
                // console.log("false")
            }
        }else{
            // console.log("false");
        }
    },[])

    // console.log(varStorage);

    // useEffect( () => {
    //     let totals = 0;
    //     if(localCart){
    //         console.log(localCart)
    //         localCart.forEach(ohyeah => {
    //             totals += ohyeah.price;
    //         })
    //     }
        
    //     setTotal(totals);
    //     console.log(totals);
        
    // },[cart])

	const [categoriesStatus, setCategoriesStatus] = useState({
        lastUpdated : null,
        status : null,
        isLoading : false
    });

    let user = localStorage.getItem("user");
    let newUser = JSON.parse(user);

	useEffect( ()=>{
        fetch("https://bookacar.herokuapp.com/products",{
            method : "GET"})
        .then(data => data.json())
        .then(products => {
           setProducts(products)
        })

        fetch("https://bookacar.herokuapp.com/dates",{
            method : "GET"
        })
        .then(data => data.json())
        .then(dates => {
            setReservedDates(dates)
        })

        fetch("https://bookacar.herokuapp.com/rooms",{
            method : "GET"})
        .then(data => data.json())
        .then(rooms => {
           setRooms(rooms)
        })

        if(user){

        fetch("https://bookacar.herokuapp.com/transactions",{
            method : "GET",
            headers : {
                "Authorization" : localStorage.getItem("token")
                }
        })
        .then(data => data.json())
        .then(transactions => {
           setTransactions(transactions)
        })

       
            if(newUser.role === "admin"){
                fetch("https://bookacar.herokuapp.com/categories",{
                method : "GET",
                headers : {
                    "Authorization" : localStorage.getItem("token")
                    }
                })
                .then(data => data.json())
                .then(categories => {
                   setCategories(categories)
                   setCategoriesStatus({isLoading : false})
                })
            }
        }
        
        

    },[categoriesStatus.isLoading]);

	const handleChangeCategoriesStatus = (status) => {
        setCategoriesStatus(status)
        
    }  

    const handleAddToCart = (product,type,number,rumId) => {
        // console.log(product);
        // console.log(type);
        // console.log(number);
        let matched = cart.find(item => {
            return item.roomNum === number;
        })
        if(!matched){
            setCart([...cart,{...product,roomId : rumId, roomType : type, roomNumber : number}]);
            // localStorage.setItem("cart",JSON.stringify(cart));
        }
        // console.log(matched);
    }

    useEffect( () =>{
        if(cart.length < 1){

        }else{
            localStorage.setItem("cart",JSON.stringify(cart));
        }

        // console.log(cart)
    },[cart])

    const addDate = (checkin,checkout) => {
        // console.log(checkin,checkout);
        // let dates = [startDate,endDate];
        setDate({startDate : checkin, endDate : checkout});
    }

    const handleClearCart = () =>{
        setCart([])
        localStorage.removeItem("cart");
    }

    const handleRemoveItem = (item) => {
        // let a = cart.filter(carts=>
        //     carts !== item
        // )
        // setCart(a)
        const newCart = localStorage.getItem("cart");
        let localCart =JSON.parse(newCart);
        let newLocalCart = [];

        localCart.forEach(cart =>{
            if(cart.roomNumber !== item.roomNumber){
                newLocalCart.push(cart);
            }
        })
        // console.log(item.roomNumber)
        // console.log(newLocalCart);
        localStorage.setItem("cart",JSON.stringify(newLocalCart));
        window.location.href="/cart";
    }

    // console.log(date)
    if(newUser){
        if(newUser.role === "user"){
            return (
                <Router>
                    <Navbar/>
                    <Switch>
                        <Route exact path="/">
                            <About/>
                        </Route>
                        <Route exact path="/contact">
                            <Contact/>
                        </Route>
                        <Route exact path="/book">
                            <Home products = {products} addToCart={handleAddToCart} transactions={transactions} date={addDate} reservedDates={reservedDates} rooms={rooms} dates={date} cart={cart}/>
                        </Route>
                        <Route path="/cart">
                            <Cart carts={cart} date={date} handleClearCart={handleClearCart} handleRemoveItem={handleRemoveItem} total={total} products={products}/>
                        </Route>
                        <Route path="/transactions">
                            <Transaction transactions={transactions} rooms={rooms}/>
                        </Route>
                        {
                            newUser.role === "user" ?
                                <Redirect to="book"/>
                            :
                            ""
                        }
                    </Switch>
                    <Footer/>
                </Router>
            );
        }else{
            return (
                <Router>
                    <Navbar/>
                    <Switch>
                        <Route exact path="/">
                            <About/>
                        </Route>
                        <Route path="/transactions">
                            <Transaction transactions={transactions} rooms={rooms}/>
                        </Route>
                        <Route path="/admin-panel">
                            <AdminPanel categories = {categories} handleChangeCategoriesStatus = {handleChangeCategoriesStatus} categoriesStatus = {categoriesStatus} products={products} rooms={rooms}/>
                        </Route>
                        {
                            newUser.role === "admin" ?
                                <Redirect to=""/>
                            :
                            ""
                        }
                    </Switch>
                    <Footer/>
                </Router>
            );
        }
    }else{
        return (
            <Router>
                <Navbar/>
                <Switch>
                    <Route exact path="/">
                        <About/>
                    </Route>
                    <Route path="/contact">
                        <Contact/>
                    </Route>
                    <Route path="/register">
                        <RegisterForm/>
                    </Route>
                    <Route path="/login">
                        <LoginForm/>
                    </Route>
                    <Route path="/book">
                        <Home products = {products} addToCart={handleAddToCart} transactions={transactions} date={addDate} reservedDates={reservedDates} rooms={rooms} dates={date} cart={cart}/>
                    </Route>
                    {
                        newUser ? 
                            ""
                        :
                            <Redirect to="login"/>
                    }
                </Switch>
                <Footer/>
            </Router>
        );
    }
}

export default App;


/*
{
newUser ?
    newUser.role === "user" ?
        <Route exact path="/book">
            <Home products = {products} addToCart={handleAddToCart} transactions={transactions} date={addDate} reservedDates={reservedDates} rooms={rooms} dates={date}/>
        </Route>
    :
    <Redirect to="admin-panel"/>
:
<Redirect to="login"/>
}
{
newUser ? 
    newUser.role == "user" ? 
        <Route path="/cart">
            <Cart carts={cart} date={date} handleClearCart={handleClearCart} handleRemoveItem={handleRemoveItem} total={total}/>
        </Route>
        :
        <Redirect to=""/>
:
<Redirect to="login"/>
}
{
newUser ?
    <Route path="/transactions">
        <Transaction transactions={transactions} rooms={rooms}/>
    </Route>
:
<Redirect to="login"/>
}
{
newUser ?
    newUser.role === "admin" ?
        <Route path="/admin-panel">
            <AdminPanel categories = {categories} handleChangeCategoriesStatus = {handleChangeCategoriesStatus} categoriesStatus = {categoriesStatus} products={products}/>
        </Route>
    :
    <Redirect to="book"/> 
:
<Redirect to="login"/>                    
}



*/

/*

<Route exact path="/">
    <About/>
</Route>
<Route path="/book">
    <Home products = {products} addToCart={handleAddToCart} transactions={transactions} date={addDate} reservedDates={reservedDates} rooms={rooms} dates={date}/>
</Route>
<Route path="/register">
    <RegisterForm/>
</Route>
<Route path="/login">
    <LoginForm/>
</Route>
*/